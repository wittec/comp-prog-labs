fun main() {
    val br = System.`in`.bufferedReader()
    val n = br.readLine().toInt()
    val s = IntArray(n) { br.read() - 'A'.toInt() }
    var badChar = 1
    var ans = 0
    for (i in n - 1 downTo 1) {
        if (s[i] == badChar) {
            ans++
            if (s[i - 1] == badChar) {
                badChar = 1 xor badChar
            }
        }
    }
    if (s.first() == badChar) ans++
    println(ans)
}