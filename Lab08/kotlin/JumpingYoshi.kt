import java.io.BufferedReader
import kotlin.math.max

fun main() {
    val br = System.`in`.bufferedReader()
    val n = br.readInt()
    val nums = IntArray(n) { br.readInt() }
    // the list of all nodes by the sum of its location and number of spots
    val bySum = Array(n) { mutableListOf<Int>() }
    // tje list of all nodes by the difference of its location and number of spots
    val byDif = Array(n) { mutableListOf<Int>() }
    // add each node to the sum/dif lists
    for (i in 0 until n) {
        val sum = nums[i] + i
        if (sum < n) bySum[sum].add(i)
        val dif = i - nums[i]
        if (dif >= 0) byDif[dif].add(i)
    }
    var ans = 0
    // array for checking if we've already processed the sum/difference
    val seenSum = BooleanArray(n)
    val seenDif = BooleanArray(n)

    // queue for bfs
    val queue = java.util.ArrayDeque<Int>()
    if (nums[0] < n) {
        // add the sum of the first stone as a difference
        seenDif[nums[0]] = true
        queue += nums[0]
    }
    while (queue.isNotEmpty()) {
        val node = queue.poll()

        // nonnegative numbers are differences
        if (node >= 0) {
            if (byDif[node].isNotEmpty()) {
                if (!seenSum[node]) { // add this difference as a sum
                    seenSum[node] = true
                    queue += -(node + 1)
                }
            }
            for (adj in byDif[node]) { // for everything with this difference, add their sum as a difference
                ans = max(ans, adj) // update answer possibly
                val sum = nums[adj] + adj
                if (sum < n && !seenDif[sum]) {
                    seenDif[sum] = true
                    queue += sum
                }
            }
        } else { // negative numbers are sums
            val sum = -(node + 1) // convert it to the nonnegative actual sum
            if (!seenDif[sum]) { // add this sum as a difference
                seenDif[sum] = true
                queue += sum
            }
            for (adj in bySum[sum]) { // for everything with this sum, add their difference as a sum
                ans = max(ans, adj) // update answer possibly
                val dif = adj - nums[adj]
                if (dif >= 0 && !seenSum[dif]) {
                    seenSum[dif] = true
                    queue += -(dif + 1)
                }
            }
        }
    }
    println(ans)

}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}
