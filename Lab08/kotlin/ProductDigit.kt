import kotlin.math.max

// precalulated values
private val facts = LongArray(21)
private val tenPows = LongArray(16)

// this is the counting function for when we just want to permute all of the digits. i.e. we already know it fits in our bound regardless of what we do
private fun countWays(counts: IntArray): Long {
    var denom = 1L
    var tot = 0
    for (i in 1 until 9) { // find the total number of digits
        tot += counts[i]
        denom *= facts[counts[i]] // start calculating the denominator
    }
    val base = facts[tot + counts[0]] / denom // base counts without taking into account the 0/1 digits
    var ans = 0L
    for (i in 0..counts[0]) { // for every possible split of 0's and 1's, calculate the number of ways
        ans += base / (facts[i] * facts[counts[0] - i])
    }
    return ans
}


private fun count(high: Long, counts: IntArray, idx: Int): Long {
    if (idx == -1) return 1L // if we perfectly matched the bound, yay we found a number
    var ans = 0L
    when (val needed = (high / tenPows[idx]).toInt()) { // what's the most significant digit ?
        0 -> { // need to handle 0 and 1 separately since they're both represented the same way in the multiset
            if (counts[0] != 0) {// if we have the digit
                counts[0]--
                ans += count(high, counts, idx - 1) // go to next digit
                counts[0]++
            }
        }
        1 -> {
            if (counts[0] != 0) {// if we have the digit
                counts[0]--
                ans += countWays(counts) // count the ways assuming it's a 0, then count the ways assuming it's a 1
                ans += count(high - tenPows[idx], counts, idx - 1) // go to next digit
                counts[0]++
            }
        }
        else -> {
            if (counts[0] != 0) { // if we have the digit
                counts[0]--
                ans += countWays(counts) shl 1 // times 2 because this digit count be a 0 or a 1
                counts[0]++
            }
            for (nextDig in 1 until needed - 1) {
                if (counts[nextDig] != 0) { // if we have the digit
                    counts[nextDig]-- // remove the digit and count the permutations
                    ans += countWays(counts)
                    counts[nextDig]++
                }
            }
            if (counts[needed - 1] != 0) { // if we have the relevant digit in the multiset
                counts[needed - 1]--
                ans += count(high - needed * tenPows[idx], counts, idx - 1) // go to next digit
                counts[needed - 1]++
            }
        }
    }
    return ans
}

private fun generate( // function for generating all of the multisets of digits
    curCounts: IntArray, // the current multiset
    numsLeft: Int, // the numbers left to be added
    prevNum: Int, // the number most recently added
    low: Long, // the low number of our range
    high: Long, // the high number of our range
    ret: LongArray // our final answer
) {
    if (numsLeft == 0) {
        var ans = 1L
        for (i in 0 until 9) {
            repeat(curCounts[i]) {
                ans *= i + 1
            }
        }
        while (ans >= 10) { // calculate the digit product for this multiset
            var next = 1L
            while (ans != 0L) {
                next *= max(ans % 10, 1L)
                ans /= 10
            }
            ans = next
        }
        ret[(ans - 1).toInt()] += count(high, curCounts, 14) // calculate count [0, high]
        ret[(ans - 1).toInt()] -= count(low, curCounts, 14) // calculate count [0, low]
    } else {
        // iterate over numbers at least as big as the most recently added number
        for (num in prevNum..9) {
            curCounts[num - 1]++
            generate(curCounts, numsLeft - 1, num, low, high, ret)
            curCounts[num - 1]--
        }
    }
}

fun main() {
    tenPows[0] = 1
    for (i in 1..15) { // precalcuate some useful quantities
        tenPows[i] = tenPows[i - 1] * 10
    }
    // including factorials
    facts[0] = 1L
    for (i in 1..20) {
        facts[i] = facts[i - 1] * i
    }


    val br = System.`in`.bufferedReader()
    val ans = LongArray(9)
    var (l, r) = br.readLine().split(" ").map { it.toLong() }
    if (r == tenPows[15]) { 
        // the code doesn't quite work for a 16 digit number, and it'd be too much of a performance hit to consider multisets of size 16
        // instead we just treat it as a special case
        r--
        ans[0]++
    }
    // important part here is that we subtract 1 from the lower bound
    generate(IntArray(9), 15, 1, l - 1, r, ans)

    // this problem is a pain
    println(ans.joinToString(" "))
}
