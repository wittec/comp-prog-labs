import java.io.BufferedReader

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val l = br.readLong()
    val a = br.readLong()
    val b = br.readLong()
    val t = br.readLong() * a * b // adjust the times to make sure all other times are integers
    val r = br.readLong() * a * b
    val n = br.readInt()
    val nums = LongArray(n + 1)
    for (i in 0 until n) {
        nums[i] = br.readLong() * a * b // adjust the distances by a and b
    }
    nums[n] = l * a * b // also adjusting this distance
    val prev = IntArray(n + 1) { -1 }
    val dp = LongArray(n + 1) { i -> nums[i] / a } // set the default to walking straight here without buying anything before

    var lookahead = 0 // the closest node a full coffee away

    fun calcTime(dist: Long): Long { // function for calculating the time to walk a distance assuming we buy a coffee right before starting
        return if (dist <= t * a) {
            dist / a
        } else if (dist <= t * a + r * b) {
            t + (dist - t * a) / b
        } else {
            r + (dist - r * b) / a
        }
    }
    for (i in 0 until n) {
        while (lookahead < n && nums[lookahead] - nums[i] <= t * a + r * b) { // walk the index up until it's far enough away
            lookahead++
        }
        if (lookahead - 1 != i) { // if there's actually a relevant node before we finish drinking the coffee
            val new = dp[i] + calcTime(nums[lookahead - 1] - nums[i])
            if (new < dp[lookahead - 1]) { // if the new time is better than the old time, update the values
                dp[lookahead - 1] = new
                prev[lookahead - 1] = i
            }
        }
        val new = dp[i] + calcTime(nums[lookahead] - nums[i])
        if (new < dp[lookahead]) { // if the new time is better that the old time, update the values 
            dp[lookahead] = new
            prev[lookahead] = i

        }
    }
    val ans = mutableListOf<Int>() // list of shops to visit
    var cur = n
    while (prev[cur] != -1) { // make sure we don't add our fake extra shop at the departure gate
        cur = prev[cur]
        ans += cur
    }
    bw.append("${ans.size}\n")
    bw.append(ans.joinToString(" ")) // important that they don't care about the order we output the shops
    bw.flush()

}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}

private fun BufferedReader.readLong(): Long {
    var ret = read().toLong()
    while (ret <= SPACE_INT) {
        ret = read().toLong()
    }
    val neg = ret == '-'.toLong()
    if (neg) {
        ret = read().toLong()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }
    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}
