import kotlin.math.sign
import kotlin.system.exitProcess

fun main() { 
    val br = System.`in`.bufferedReader()
    val n = br.readLine().toInt()
    val lines = Array(n) { // read in all of the segments
        val (x1, y1, x2, y2) = br.readLine().split(" ").map { it.toLong() }
        AliasSegment(
            AliasIntPoint(x1, y1), AliasIntPoint(x2, y2)
        )
    }
    val points = mutableListOf<AliasRationalPoint>() // list of intersection points
    for (i in 0 until n) {
        for (j in i + 1 until n) { // iterate through every pair of segments
            val intersect = lines[i].intersects(lines[j]) // find the intersection point if it exists
            if (intersect != null) points.add(intersect) // if it exists, add it to the list
        }
    }

    points.sort()
    var ans = 0
    if (points.isNotEmpty()) ans++ // special case handling for no intersections

    for (i in 1 until points.size) { // checking distinct points
        if (points[i] != points[i - 1]) ans++
    }
    println(ans)

}

private data class AliasSegment(val p1: AliasIntPoint, val p2: AliasIntPoint) {
    val dir = p2 - p1 // the vector from p1 to p2


    // check if the points on the other segment lie on different sides of this segment 
    // (lying on the line is considered a different side than left or right)
    fun splits(other: AliasSegment): Boolean {
        val c1 = this.side(other.p1)
        val c2 = this.side(other.p2)
        return c1 != c2
    }

    // -1, 1, or 0 depending on which side of the segment the point lies
    // it's 0 if the point lies along the same infinite line
    fun side(p: AliasIntPoint): Int = dir.cross(this.p2 - p).sign 

    operator fun unaryMinus(): AliasSegment {
        return AliasSegment(p2, p1)
    }

    // checking if the slopes are the same
    // same as dy1/dx1 = dy2/dx2 but without the risk of dividing by 0
    fun isParallel(other: AliasSegment): Boolean {
        return other.dir.y * this.dir.x == this.dir.y * other.dir.x
    }

    fun intersects(other: AliasSegment): AliasRationalPoint? {
        if (this.isParallel(other)) { // special handling for segments with the same slope
            if (this.side(other.p1) != 0) return null // if they don't lie on the same infinite line, they don't intersect

            // this segments points, first one is the one whose dot product with the slope is a, second is same for b
            val tp = arrayOf(this.p1, this.p2) 
            var flipped = false

            var a = this.p1.dot(dir)
            var b = this.p2.dot(dir)
            // getting a and b in sorted order
            if (b < a) {
                val t = a
                a = b
                b = t
                flipped = true
                val t2 = tp[0]
                tp[0] = tp[1]
                tp[1] = t2
            }
            var c = other.p1.dot(dir)
            var d = other.p2.dot(dir)
            // getting c and d in sorted order
            if (d < c) {
                val t = c
                c = d
                d = t
            }
            // if the later point of one segment equals the earlier point on the other segment, then they intersect at that single point only
            if (b == c) {
                return (if (flipped) tp[0] else tp[1]).asRationalPoint()
            }
            if (d == a) {
                return (if (flipped) tp[1] else tp[0]).asRationalPoint()
            }
            // if the later of one segment is before the earlier of the other, they don't intersect at all
            if (b < c) return null
            if (d < a) return null

            // otherwise they have infinitely many intersection points
            println(-1)
            exitProcess(0)
        }

        // if they don't have the same slope, than they intersect exactly when each segment splits the other
        return if (this.splits(other) && other.splits(this)) {
            // math to calculate the intersection point
            val denom = this.dir.cross(other.dir)
            val thisCross = this.p1.cross(this.p2)
            val otherCross = other.p1.cross(other.p2)
            val xNum = this.dir.x * otherCross - other.dir.x * thisCross
            val yNum = this.dir.y * otherCross - other.dir.y * thisCross
            // return the intersection point as a pair of rational numbers
            AliasRationalPoint(AliasRational.of(xNum, denom), AliasRational.of(yNum, denom))
        } else {
            null
        }
    }
}

// class 
private data class AliasIntPoint(val x: Long, val y: Long) : Comparable<AliasIntPoint> {
    operator fun minus(other: AliasIntPoint): AliasIntPoint {
        return AliasIntPoint(this.x - other.x, this.y - other.y)
    }

    // cross product with another point, technically returns the z coordinate of the cross product assuming that z coordinate of these points is 0
    fun cross(other: AliasIntPoint): Long {
        return this.x * other.y - other.x * this.y
    }

    // dot product with another point
    fun dot(other: AliasIntPoint): Long {
        return this.x * other.x + this.y * other.y
    }

    fun asRationalPoint(): AliasRationalPoint {
        return AliasRationalPoint(AliasRational.of(x), AliasRational.of(y))
    }

    // to provide a stable ordering
    override fun compareTo(other: AliasIntPoint): Int {
        val xComp = this.x.compareTo(other.x)
        return if (xComp == 0) {
            this.y.compareTo(other.y)
        } else {
            xComp
        }
    }
}

private data class AliasRationalPoint(val x: AliasRational, val y: AliasRational) : Comparable<AliasRationalPoint> {
    operator fun minus(other: AliasRationalPoint): AliasRationalPoint {
        return AliasRationalPoint(this.x - other.x, this.y - other.y)
    }

    fun cross(other: AliasRationalPoint): AliasRational {
        return this.x * other.y - other.x * this.y
    }

    fun dot(other: AliasRationalPoint): AliasRational {
        return this.x * other.x + this.y * other.y
    }

    override fun compareTo(other: AliasRationalPoint): Int {
        val xComp = this.x.compareTo(other.x)
        return if (xComp == 0) {
            this.y.compareTo(other.y)
        } else {
            xComp
        }
    }
}

private class AliasRational private constructor(val numer: Long, val denom: Long = 1L) : Comparable<AliasRational> {

    operator fun minus(other: AliasRational): AliasRational {
        val g = gcd(this.denom, other.denom)
        return of(this.numer * (other.denom / g) - other.numer * (this.denom / g), (this.denom / g) * other.denom)
    }

    operator fun plus(other: AliasRational): AliasRational {
        val g = gcd(this.denom, other.denom)
        return of(this.numer * (other.denom / g) + other.numer * (this.denom / g), (this.denom / g) * other.denom)
    }

    fun sign(): Int = numer.sign

    operator fun times(other: AliasRational): AliasRational {
        val g1 = gcd(this.numer, other.denom)
        val g2 = gcd(other.numer, this.denom)
        return of((this.numer / g1) * (other.numer / g2), (this.denom / g2) * (other.denom / g1))
    }

    operator fun times(mult: Int): AliasRational {
        return of(mult * this.numer, this.denom)
    }

    operator fun times(mult: Long): AliasRational {
        return of(mult * this.numer, this.denom)
    }

    fun reciprocal(): AliasRational {
        return if (this.sign() == -1) {
            AliasRational(-denom, -numer)
        } else {
            AliasRational(denom, numer)
        }
    }

    fun absoluteValue(): AliasRational {
        return if (numer < 0) -this else this
    }

    operator fun div(other: AliasRational): AliasRational {
        return this * other.reciprocal()
    }


    override fun toString(): String = "$numer/$denom"
    operator fun unaryMinus(): AliasRational = AliasRational(-numer, denom)


    companion object {
        fun of(numer: Long, denom: Long = 1L): AliasRational {
            val g = gcd(numer, denom)
            return if (denom < 0) AliasRational(-numer / g, -denom / g) else AliasRational(numer / g, denom / g)
        }

        private fun gcd(p: Long, q: Long): Long {
            return if (p < 0) gcd(-p, q)
            else if (q < 0) gcd(p, -q)
            else if (p == 0L) q
            else if (q == 0L) p
            else if (p and 1L == 0L && q and 1L == 0L) gcd(p shr 1, q shr 1) shl 1
            else if (p and 1L == 0L) gcd(p shr 1, q)
            else if (q and 1L == 0L) gcd(p, q shr 1)
            else if (p > q) gcd((p - q) shr 1, q)
            else gcd(p, (q - p) shr 1)
        }
    }

    // doesn't technically sort by it's value, just provides some consistent ordering. Trying to sort by value results in some overflow errors
    override fun compareTo(other: AliasRational): Int {
        return if (this.numer == other.numer) this.denom.compareTo(other.denom)
        else this.numer.compareTo(other.numer)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AliasRational

//        return this.numer * other.denom == this.denom * other.numer
        if (numer != other.numer) return false
        if (denom != other.denom) return false
//
        return true
    }

    override fun hashCode(): Int {
        var result = numer.hashCode()
        result = 31 * result + denom.hashCode()
        return result
    }
}
