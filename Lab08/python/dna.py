from http.client import OK
from sys import stdin


n = int(stdin.readline())

line = stdin.readline().strip()
line = list(line)
acount = 0
bcount = 0
flip_count = 0
for i in range(len(line) - 1, -1, -1):
    if line[i] == 'A':
        acount += 1
    if line[i] == 'B':
        bcount += 1
        flip_count += 1

ans = 0

flag = True

for i in range(len(line) - 1, -1, -1):
    if flag:
        if line[i] == 'B':
            ans += 1
            if i == 0 or line[i - 1] == 'B':
                flag = False
    else:
        if line[i] == 'A':
            ans += 1
            if i == 0 or line[i - 1] == 'A':
                flag = True

print(ans)



