from sys import stdin
import heapq
from collections import deque

n = int(stdin.readline())
#n = 1
kMove = [[1, 2], [-1, 2],[1, -2],[-1, -2],[2,1],[-2,1],[2,-1],[-2,-1]]

lookup = {
    'a' : 0,
    'b' : 1,
    'c' : 2,
    'd' : 3,
    'e' : 4,
    'f' : 5,
    'g' : 6,
    'h' : 7
 }

rev_lookup = 'abcdefgh'

for i in range(n):
    board = [[-1 for i in range(8)] for _ in range(8)]
    starting_point = stdin.readline()
    init_y = int(starting_point[1]) - 1
    init_x = lookup[starting_point[0]] 
    queue = deque()
    queue.append((init_y,init_x))
    board[init_y][init_x] = 0
    while len(queue) != 0:
        (y, x) = queue.popleft()
        for dy, dx in kMove:
            new_x = x + dx
            new_y = y + dy
            if new_x in range(8) and new_y in range(8) and board[new_y][new_x] == -1:
                board[new_y][new_x] = board[y][x] + 1
                queue.append((new_y, new_x))
    ans = []
    max_dist = 0
    #max_dist = max(board)
    for row in range(7,-1, -1):
        for col in range(8):
            if board[row][col] > max_dist:
                max_dist = board[row][col]
                ans = []
            if board[row][col] == max_dist:
                ans.append(rev_lookup[col] + str(row + 1))
    print(f'{max_dist} {" ".join(ans)}')