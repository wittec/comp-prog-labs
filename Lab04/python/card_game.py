from sys import stdin
from collections import deque

n = int(stdin.readline())
arr = list(map(int, stdin.readline().split()))
arr = sorted(arr)
q = int(stdin.readline())

def recursion(arr, low, high, target):
    if low == high:
        return low
    mid = (low + high) // 2
    if arr[mid] < target:
        return recursion(arr, mid + 1, high, target)
    else:
        return recursion(arr, low, mid, target)


ans = []
for i in range(q):
    l,r = map(int, stdin.readline().split())
    start = recursion(arr, 0, n, l)
    stop = recursion(arr, start, n, r + 1)
    ans.append(str(stop - start))
print('\n'.join(ans))
    
