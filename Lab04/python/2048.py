from sys import stdin
from collections import deque
direction = [[0,-1], [-1,0], [0, 1], [1,0]]
queue = deque()

merged = [[False for _ in range(4)] for _ in range(4)]
grid = []
for i in range(4):
    grid.append(list(map(int, stdin.readline().split())))
move = int(stdin.readline())

if move == 0:
    for i in range(4):
        for j in range(1, 4):
            if grid[i][j] != 0:
                col = j - 1
                while col >= 0 and grid[i][col] == 0:
                    col -= 1
                if col == -1:
                    grid[i][0] = grid[i][j]
                    grid[i][j] = 0
                elif grid[i][col] == grid[i][j] and not (merged[i][col]):
                    grid[i][col] = grid[i][col] * 2
                    grid[i][j] = 0
                    merged[i][col] = True
                elif col + 1 != j:
                    grid[i][col + 1] = grid[i][j]
                    grid[i][j] = 0
elif move == 2:
    for i in range(4):
        for j in range(2, -1, -1):
            if grid[i][j] != 0:
                col = j + 1
                while col < 4 and grid[i][col] == 0:
                    col += 1
                if col == 4:
                    grid[i][3] = grid[i][j]
                    grid[i][j] = 0
                elif grid[i][col] == grid[i][j] and not (merged[i][col]):
                    grid[i][col] = grid[i][col] * 2
                    grid[i][j] = 0
                    merged[i][col] = True
                elif col - 1 != j:
                    grid[i][col - 1] = grid[i][j]
                    grid[i][j] = 0
elif move == 3:
    for i in range(4):
        for j in range(2, -1, -1):
            if grid[j][i] != 0:
                row = j + 1
                while row < 4 and grid[row][i] == 0:
                    row += 1
                if row == 4:
                    grid[3][i] = grid[j][i]
                    grid[j][i] = 0
                elif grid[row][i] == grid[j][i] and not (merged[row][i]):
                    grid[row][i] = grid[row][i] * 2
                    grid[j][i] = 0
                    merged[row][i] = True
                elif row - 1 != j:
                    grid[row - 1][i] = grid[j][i]
                    grid[j][i] = 0
elif move == 1:
    for i in range(4):
        for j in range(1, 4):
            if grid[j][i] != 0:
                row = j - 1
                while row >= 0 and grid[row][i] == 0:
                    row -= 1
                if row == -1:
                    grid[0][i] = grid[j][i]
                    grid[j][i] = 0
                elif grid[row][i] == grid[j][i] and not (merged[row][i]):
                    grid[row][i] = grid[row][i] * 2
                    grid[j][i] = 0
                    merged[row][i] = True
                elif row + 1 != j:
                    grid[row + 1][i] = grid[j][i]
                    grid[j][i] = 0

for i in range(4):
    print(' '.join(map(str, grid[i])))


