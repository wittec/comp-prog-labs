from sys import stdin
from collections import deque
d = [[0,1], [1,0], [0, -1], [-1, 0]]

r,c = map(int, stdin.readline().split())
queue = deque()

grid = [list(stdin.readline().strip()) for _ in range(r)]
ans = 0
for i in range(r):
    for j in range(c):
        if grid[i][j] == 'L':
            grid[i][j] = 'X'
            ans += 1
            queue.append((i, j))
            while len(queue) != 0:
                (row, col) = queue.popleft()
                for d_row, d_col in d:
                    new_col = col + d_col
                    new_row = row + d_row
                    if new_col in range(c) and new_row in range(r) and (grid[new_row][new_col] == 'L' or grid[new_row][new_col] == 'C') :
                        grid[new_row][new_col] = 'X'
                        queue.append((new_row, new_col))
print(ans)
