

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class P2048 {

    private static final int LEFT = 0;
    private static final int UP = 1;
    private static final int RIGHT = 2;
    private static final int DOWN = 3;

    public static void main(String[] args) throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        int[][] grid = new int[4][4];
        for (int i = 0; i < grid.length; i++) {
            String[] line = in.readLine().split(" ");
            for (int e = 0; e < line.length; e++) {
                grid[i][e] = Integer.parseInt(line[e]);
            }
        }
        int direction = Integer.parseInt(in.readLine());
        for (int d1 = 0; d1 < grid.length; d1++) {
            int nonZeroCount = 0;
            for (int d2 = 0; d2 < grid.length; d2++) {
                if (accessGrid(d1, d2, direction, grid) != 0) {
                    setGrid(d1, nonZeroCount, direction, grid, accessGrid(d1, d2, direction, grid));
                    if (nonZeroCount < d2) {
                        setGrid(d1, d2, direction, grid, 0);
                    }
                    nonZeroCount++;
                }
            }
            int readIndex = 0;
            int writeIndex = 0;

            while (writeIndex < grid.length) {
                if (readIndex + 1 < nonZeroCount) {
                    if (accessGrid(d1, readIndex, direction, grid) == accessGrid(d1, readIndex + 1, direction, grid)) {
                        setGrid(d1, writeIndex, direction, grid, 2 * accessGrid(d1, readIndex, direction, grid));
                        readIndex++;
                    } else {
                        setGrid(d1, writeIndex, direction, grid, accessGrid(d1, readIndex, direction, grid));
                    }
                } else if (readIndex + 1 == nonZeroCount) {
                    setGrid(d1, writeIndex, direction, grid, accessGrid(d1, readIndex, direction, grid));
                } else {
                    setGrid(d1, writeIndex, direction, grid, 0);
                }
                readIndex++;
                writeIndex++;
            }
        }

        printGrid(grid);

    }

    private static void printGrid(int[][] grid) {
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                System.out.format("%d ", grid[row][col]);
            }
            System.out.format("\n");
        }
    }

    private static int accessGrid(int d1, int d2, int direction, int[][] grid) {
        switch (direction) {
            case LEFT:
                return grid[d1][d2];
            case RIGHT:
                return grid[d1][grid.length - 1 - d2];
            case UP:
                return grid[d2][d1];
            case DOWN:
                return grid[grid.length - 1 - d2][d1];
            default:
                return -1;
        }
    }

    private static void setGrid(int d1, int d2, int direction, int[][] grid, int val) {
        switch (direction) {
            case LEFT:
                grid[d1][d2] = val;
                break;
            case RIGHT:
                grid[d1][grid.length - 1 - d2] = val;
                break;
            case UP:
                grid[d2][d1] = val;
                break;
            case DOWN:
                grid[grid.length - 1 - d2][d1] = val;
                break;
        }
    }


}
