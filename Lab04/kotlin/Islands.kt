import java.util.ArrayDeque

fun main() {
    val dRow = intArrayOf(0, 1, 0, -1)
    val dCol = intArrayOf(1, 0, -1, 0)
    val br = System.`in`.bufferedReader()
    val (r, c) = br.readLine().split(" ").map { it.toInt() }
    val grid = Array(r) {
        val row = CharArray(c) { br.read().toChar() }
        br.read()
        row
    }
    val queue = ArrayDeque<Pair<Int, Int>>()
    var ans = 0
    for (row in 0 until r) {
        for (col in 0 until c) {
            if (grid[row][col] == 'L') {
                ans++
                queue += row to col
                grid[row][col] = 'W'
                while (queue.isNotEmpty()) {
                    val (cRow, cCol) = queue.poll()
                    for (dir in 0 until 4) {
                        val newRow = cRow + dRow[dir]
                        val newCol = cCol + dCol[dir]
                        if (newRow in 0 until r && newCol in 0 until c && grid[newRow][newCol] != 'W') {
                            grid[newRow][newCol] = 'W'
                            queue += newRow to newCol
                        }
                    }
                }
            }
        }
    }
    println(ans)
}