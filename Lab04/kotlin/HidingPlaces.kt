import java.util.ArrayDeque
import kotlin.math.max

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val n = br.readLine().toInt()
    repeat(n) {
        val input = br.readLine()
        val dists = Array(8) { IntArray(8) { -1 } }
        val sRow = 8 - (input[1] - '0')
        val sCol = input[0] - 'a'
        val queue = ArrayDeque<Pair<Int, Int>>()
        dists[sRow][sCol] = 0
        queue += sRow to sCol
        var maxDist = 0
        while (queue.isNotEmpty()) {
            val (row, col) = queue.poll()
            maxDist = max(maxDist, dists[row][col])
            for (dRow in 1..2) {
                val dCol = 3 - dRow
                for (rowM in -1..1 step 2) {
                    for (colM in -1..1 step 2) {
                        val newRow = row + rowM * dRow
                        val newCol = col + colM * dCol
                        if (newRow in 0 until 8 && newCol in 0 until 8) {
                            if (dists[newRow][newCol] == -1) {
                                dists[newRow][newCol] = dists[row][col] + 1
                                queue += newRow to newCol
                            }
                        }
                    }
                }
            }
        }

        bw.append("$maxDist")
        for (row in 0 until 8) {
            for (col in 0 until 8) {
                if (dists[row][col] == maxDist) {
                    bw.append(" ${'a' + col}${8 - row}")
                }
            }
        }
        bw.newLine()
    }
    bw.flush()
}