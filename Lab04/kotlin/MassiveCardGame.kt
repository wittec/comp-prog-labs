import java.io.BufferedReader

private fun bs(nums: IntArray, check: Int): Int {
    var low = 0
    var high = nums.size
    while (low < high) {
        val mid = (low + high) shr 1
        if (nums[mid] < check) {
            low = mid + 1
        } else {
            high = mid
        }
    }
    return low
}

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val n = br.readInt()
    val nums = IntArray(n) { br.readInt() }.sorted().toIntArray()
    val q = br.readInt()
    repeat(q) {
        val l = br.readInt()
        val r = br.readInt() + 1
        bw.append("${bs(nums, r) - bs(nums, l)}\n")
    }
    bw.flush()
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}