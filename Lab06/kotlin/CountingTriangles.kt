import kotlin.math.sign

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    var n = br.readLine().toInt()
    while (n != 0) {
        val segments = Array(n) {
            val (x1, y1, x2, y2) = br.readLine().split(" ").map { s ->
                // converting the floating point numbers in the input to integers to avoid precision issues
                val split = s.split(".")
                // multiplying by 10^6 because at most 6 digits after the decimel point
                split.first().toLong() * 1_000_000 + if (split.size == 1) {
                    0
                } else {
                    split.last().padEnd(6, '0').toLong()
                }
            }
            CountSegment(CountPoint(x1, y1), CountPoint(x2, y2))
        }
        var ans = 0
        //iterate through every triple of segments
        for (i in 0 until n - 2) {
            for (j in i + 1 until n - 1) {
                if (!segments[i].intersects(segments[j])) continue
                for (k in j + 1 until n) {
                    if (segments[j].intersects(segments[k]) && segments[k].intersects(segments[i])) {
                        ans++
                    }
                }
            }
        }

        bw.append("$ans\n")
        n = br.readLine().toInt()
    }
    bw.flush()
}

private data class CountPoint(val x: Long, val y: Long) {
    fun cross(other: CountPoint): Long {
        return this.x * other.y - other.x * this.y
    }

    operator fun minus(other: CountPoint): CountPoint {
        return CountPoint(this.x - other.x, this.y - other.y)
    }
}

private data class CountSegment(val p1: CountPoint, val p2: CountPoint) {
    val dir = p2 - p1 // movement from p1 to p2
    fun splits(other: CountSegment): Boolean {
        val a = dir.cross(other.p1 - this.p2).sign 
        // determines whether you turn left, right, or straight after going from p1 to p2 to the other segment's points
        val b = dir.cross(other.p2 - this.p2).sign
        return a * b != 1 // only = 1 if both are 1 or both are -1 (i.e. both turning and in same direction)
    }

    fun intersects(other: CountSegment): Boolean {
        return this.splits(other) && other.splits(this)
    }
}
