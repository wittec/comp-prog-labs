import java.io.BufferedReader

fun main() {
    val br = System.`in`.bufferedReader()
    val n = br.readInt()
    val m = br.readInt()
    val ans = IntArray(n)
    var needed = n
    for (i in 1..m) {
        var a = br.readInt() - 1
        var b = br.readInt() - 1

        if (a > b) {
            val t = a
            a = b
            b = t
        }
        if (a + 1 == b) {
            ans[a] = i
            needed--
        }
        if (a == 0 && b == n - 1) {
            ans[b] = i
            needed--
        }

    }
    if (needed == 0) {
        println(ans.joinToString("\n"))
    } else {
        println("impossible")
    }
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}
