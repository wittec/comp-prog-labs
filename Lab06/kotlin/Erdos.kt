fun main() {
    val erdos = "PAUL_ERDOS"
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    var line = br.readLine()
    val orderedNames = mutableListOf<String>()
    val cons = HashMap<String, MutableList<String>>()

    while (line != null) {
        val names = line.split(" ")
        val auth = names.first()
        orderedNames.add(auth)
        for (i in 1 until names.size) {
            cons.getOrPut(auth) {
                mutableListOf()
            }.add(names[i])
            cons.getOrPut(names[i]) {
                mutableListOf()
            }.add(auth)
        }

        line = br.readLine()
    }
    val queue = java.util.ArrayDeque<String>()
    queue += erdos
    val dists = HashMap<String, Int>()
    dists[erdos] = 0
    while (queue.isNotEmpty()) {
        val name = queue.poll()
        val dist = dists[name]!!
        for (other in cons[name]!!) {
            if (dists[other] == null) {
                dists[other] = dist + 1
                queue += other
            }
        }
    }
    for (name in orderedNames) {
        if (dists[name] == null) {
            bw.append("$name no-connection\n")
        } else {
            bw.append("$name ${dists[name]}\n")
        }
    }
    bw.flush()

}