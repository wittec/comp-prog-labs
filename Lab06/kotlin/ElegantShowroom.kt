fun main() {
    val dRow = intArrayOf(0, 1, 0, -1)
    val dCol = intArrayOf(-1, 0, 1, 0)
    val br = System.`in`.bufferedReader()
    val (r, c) = br.readLine().split(" ").map { it.toInt() }
    val grid = Array(r) {
        CharArray(c) {
            br.read().toChar()
        }.also { br.read() }
    }
    val (sRow, sCol) = br.readLine().split(" ").map { it.toInt() - 1 }
    val dists = Array(r) { IntArray(c) }
    dists[sRow][sCol] = 1
    val queue = java.util.ArrayDeque<Pair<Int, Int>>()
    queue += sRow to sCol
    var ans = 0
    while (ans == 0) {
        val (row, col) = queue.poll()
        for (dir in 0 until 4) {
            val newRow = row + dRow[dir]
            val newCol = col + dCol[dir]
            if (newRow in 0 until r && newCol in 0 until c && grid[newRow][newCol] != '#' && dists[newRow][newCol] == 0) {
                if (grid[newRow][newCol] == 'c') {
                    dists[newRow][newCol] = dists[row][col] + 1
                    queue += newRow to newCol
                } else if (newRow !in 1 until r - 1 || newCol !in 1 until c - 1) {
                    ans = dists[row][col]
                } else {
                    dists[newRow][newCol] = dists[row][col]
                    queue.addFirst(newRow to newCol)
                }
            }
        }
    }
    println(ans)

}