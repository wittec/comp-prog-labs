from sys import stdin
from collections import deque
# CARDINAL DIRECTIONS 
d = [[0,1], [1,0], [0, -1], [-1, 0]]

# STORES COORDINATES OF CAR'S POSITIONS
queue = deque()

# NUMBER OF ROWS AND COLUMNS FOR SHOWROOM
r,c = map(int, stdin.readline().split())
# READ IN SHOWROOM INFORMATION
grid = [stdin.readline().strip() for _ in range(r)]
# READ IN CAR'S COORDINATES
car_r, car_c = map(int, stdin.readline().split())
car_r -= 1
car_c -= 1
# STORE NUMBER OF CARS MOVED AT EACH SPOT
dist_arr = [[-1 for _ in range(c)] for _ in range(r)]

# AT CURRENT POSITION, WE WILL MOVE THE INITIAL CAR
dist_arr[car_r][car_c] = 1
# ADD INITAL POSITION TO QUEUE
queue.append((car_r,car_c))
ans = -1
# UNTIL WE FIND THE DOOR
while ans == -1:
    # GET CURRENT POSITION
    curr_r, curr_c = queue.popleft()
    # FOR SURROUNDING DIRECTIONS
    for d_row, d_col in d:
        # GET THE NEW COORDINATES
        new_row = curr_r + d_row
        new_col = curr_c + d_col
        # IF COORDS IS IN GRID RANGE AND ISN'T A WALL AND HASN'T BEEN VISITED YET
        if new_row in range(r) and new_col in range(c) and grid[new_row][new_col] != '#' and dist_arr[new_row][new_col] == -1:
            # IF ANOTHER CAR IS PRESENT
            if grid[new_row][new_col] == 'c':
                # UPDATE NUMBER OF CARS MOVED AT THAT POSITION
                dist_arr[new_row][new_col] = dist_arr[curr_r][curr_c] + 1
                queue.append((new_row, new_col))
            # ELSE IF DOOR
            else:
                # UPDATE DISTANCE ARRAY AT POSITION
                dist_arr[new_row][new_col] = dist_arr[curr_r][curr_c]
                # ADD DOOR TO FRONT OF QUEUE 
                queue.appendleft((new_row, new_col))
                # IF IN RANGE
                if new_row not in range(1, r - 1) or new_col not in range(1, c - 1):
                    # UPDATE ANSWER
                    ans = dist_arr[curr_r][curr_c]
print(ans)

