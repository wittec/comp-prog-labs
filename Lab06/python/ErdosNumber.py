from sys import stdin
from collections import deque

# QUEUE TO STORE AUTHOR BEING EVALUATED
queue = deque()

# STORES THE ERDOS NUMBER FOR EACH AUTHOR
dists = {}
# STORES THE AUTHOR AS KEY AND CO-AUTHORS AS VALUES
connections = {}
# READ 1ST LINE
curr_line = stdin.readline().strip()
# STORE MAIN AUTHOR NAMES
names = []

# READ THE INPUT
while curr_line != "":
    arr = curr_line.split(" ")
    # MAIN AUTHOR
    auth = arr[0]
    # CO AUTHORS
    coAuth = arr[1:]
    # UPDATE CO-AUTHORS FOR MAIN AUTHORS IN DICT
    if auth in connections.keys():
        connections[auth] += coAuth
    else:
        connections[auth] = coAuth
    names.append(auth)

    # UPDATE AUTHORS FOR CO-AUTHORS
    for co in coAuth:
        if connections.get(co) == None:
            connections[co] = [auth]
        else:
            connections[co].append(auth)

    # READ NEW LINE
    curr_line = stdin.readline().strip()

# ADD FIRST NAME
queue.append("PAUL_ERDOS")
# ROOT NODE
dists["PAUL_ERDOS"] = 0
# FOR EACH NAME IN CONNECTIONS, UPDATE DISTANCE (NODE DEPTH) OF AUTHORS AND CO-AUTHORS
while len(queue) != 0:
    name = queue.pop()
    if name in connections.keys():
        for nx in connections[name]:
            if nx not in dists:
                dists[nx] = dists[name] + 1
                queue.append(nx)

# STORES NODE DEPTH FOR EACH AUTHOR
ans = ["" for _ in range(len(names))]
# PRINT ANSWER
for i in range(len(names)):
    if dists.get(names[i]) == None:
        ans[i] = f'{names[i]} no-connection'
    else:
        ans[i] = f'{names[i]} {dists[names[i]]}'
print('\n'.join(ans))
    