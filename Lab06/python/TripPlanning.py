from sys import stdin

# READ IN NUMBER OF CITIES AND TRAIN LINES
n,m = map(int, stdin.readline().split())

# CHECK FOR 2 CITIES ONLY
if n == 2:
    print("1 1")
else:
    # STORE ANSWER FOR EACH CITY
    ans = [-1 for _ in range(n)]
    # NUMBER OF CITIES LEFT TO VISIT
    needed = n

    for i in range(m):
        # READ TRAIN LINE
        a,b = map(int, stdin.readline().split())
        # GOING FROM A CITY TO ANOTHER CITY
        if min(a, b) + 1 == max(a, b):
            needed -= 1
            ans[min(a, b) - 1] = i + 1
        # GOING FROM FINAL CITY BACK TO MAIN
        elif min(a, b) == 1 and max(a, b) == n:
            needed -= 1
            ans[n-1] = i + 1
    # PRINT ANSWER
    if needed == 0:
        print('\n'.join(map(str, ans)))
    else:
        print("impossible")
