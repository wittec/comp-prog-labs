import java.io.BufferedReader
import java.util.PriorityQueue

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val t = br.readInt()
    repeat(t) {
        var milk = br.readInt()
        val n = br.readInt()
        val dists = Array(n) { IntArray(n) }
        repeat(((n - 1) * n) shr 1) {
            val u = br.readInt()
            val v = br.readInt()
            val d = br.readInt()
            dists[u][v] = d + 1
            dists[v][u] = d + 1
        }
        val pq = PriorityQueue<Pair<Int, Int>> { a, b -> a.first.compareTo(b.first) }

        val costs = IntArray(n) { Int.MAX_VALUE }
        costs[0] = 1
        pq += 1 to 0
        while (pq.isNotEmpty()) {
            val (cost, node) = pq.poll()
            if(costs[node] == cost) {
                milk -= costs[node]
                costs[node] = 0
                for(adj in 0 until n) {
                    if(dists[node][adj] < costs[adj]) {
                        costs[adj] = dists[node][adj]
                        pq += costs[adj] to adj
                    }
                }
            }
        }
        if(milk < 0) {
            bw.append("no\n")
        } else {
            bw.append("yes\n")
        }
    }
    bw.flush()

}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}