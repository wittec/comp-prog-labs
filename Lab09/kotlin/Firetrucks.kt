import java.io.BufferedReader

fun main() {
    val br = System.`in`.bufferedReader()
    val sb = StringBuilder()
    val n = br.readInt()
    val byAtt = HashMap<Int, MutableList<Int>>()
    val attSeen = HashSet<Int>()
    val personSeen = BooleanArray(n)
    val personToAtts = Array(n) { person ->
        IntArray(br.readInt()) {
            val att = br.readInt()
            byAtt.getOrPut(att) { mutableListOf() }.add(person)
            att
        }
    }
    val queue = IntArray(n)
    personSeen[0] = true
    var inIdx = 1
    var outIdx = 0
    while (outIdx < inIdx) {
        val person = queue[outIdx++]
        for (att in personToAtts[person]) {
            if (attSeen.add(att)) {
                for (adj in byAtt[att]!!) {
                    if (!personSeen[adj]) {
                        personSeen[adj] = true
                        sb.append("${person + 1} ${adj + 1} $att\n")
                        queue[inIdx++] = adj
                    }
                }
            }
        }
    }
    if (outIdx == n) {
        print(sb)
    } else {
        println("impossible")
    }
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}