import java.io.BufferedReader

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val k = br.readInt() - 1
    val parents = IntArray(100) { -1}
    var input = br.readLine().split(" ").map { it.toInt() }
    while (input.size != 1) {
        for (i in 1 until input.size) {
            parents[input[i] - 1] = input[0] - 1
        }
        input = br.readLine().split(" ").map { it.toInt() }
    }
    var cur = k
    while (cur != -1) {
        bw.append("${cur + 1} ")
        cur = parents[cur]
    }
    bw.flush()

}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}