from collections import deque
from sys import stdin

# READ IN NUMBER OF INDIVIDUALS
n = int(stdin.readline())
numToPeople = {}
processed = {}
personToNums = []
# POPULATE NUMBERS ASSOCIATED WITH AN INDIVIDUAL IN DICT
for person in range(n):
    values = list(map(int, stdin.readline().split()))
    personToNums.append(values[1:])
    for idx in range(1, len(values)):
        num = values[idx]
        if numToPeople.get(num) is not None:
            numToPeople[num].append(person)
        else:
            numToPeople[num] = [person]

queue = deque()
seen = [False for _ in range(n)]
edges = []
seen[0] = True
# ADD FIRST INDIVIDUAL
queue.append(0)
while len(queue) != 0:
    # GET CURRENT PERSON
    person = queue.pop()
    # FOR NUMBERS ASSOCIATED WITH CURRENT PERSON
    for num in personToNums[person]:
        if num not in processed:
            processed[num] = True
            # GET OTHER PEOPLE CONNECTED TO THIS NUMBER
            for otherPerson in numToPeople[num]:
                if not seen[otherPerson]:
                    seen[otherPerson] = True
                    # STORE CONNECTION
                    edges.append(f'{person + 1} {otherPerson + 1} {num}')
                    queue.append(otherPerson)
# PRINT OUTPUT
if len(edges) == n - 1:
    print('\n'.join(edges))
else:
    print("impossible")



