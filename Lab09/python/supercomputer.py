from sys import stdin

# RETURNS SUM OF BITTREE
def getsum(BITTree,i):
    s = 0 
    i = i+1
    while i > 0:
        s += BITTree[i - 1]
        i -= i & (-i)
    return s

# UPDATES THE BITTREE
def updatebit(BITTree , n , i ,v):

    i += 1
    while i <= n:
        BITTree[i - 1] += v
        i += i & (-i)

# READ IN NUMBER OF BITS AND NUMBER OF QUERIES
n,k = map(int, stdin.readline().split())

# ARRAY OF NUMBER OF BITS
arr = [0 for _ in range(n)]
# FENWICK TREE
fenwick = [0 for _ in range(n)]
# FOR EACH QUERY
for i in range(k):
    values = stdin.readline().split()
    # IF A BIT NEEDS TO BE FLIPPED
    if values[0] == 'F':
        index = int(values[1]) - 1
        if arr[index] == 0:
            updatebit(fenwick, n, index, 1)
            arr[index] = 1
        else:
            updatebit(fenwick, n, index, -1)
            arr[index] = 0
    # ELSE IF BITS 1 NEED TO BE COUNTED IN A GIVEN RANGE
    else:
        left = int(values[1]) - 1
        right = int(values[2]) - 1
        print(getsum(fenwick, right) - getsum(fenwick, left - 1), end='\n')
   


