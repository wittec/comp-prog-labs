from sys import stdin
from queue import PriorityQueue

# READ IN TEST CASES
test_cases = int(stdin.readline())

# FOR EACH TEST CASE
for _ in range(test_cases):
    # READ IN AMOUNT OF MILK AND NUMBER OF CATS
    m,c = map(int, stdin.readline().split())
    # GET COMBINATION OF CATS
    num =  (c * (c - 1)) // 2
    # 2D ARRAY TO STORE CONNECTION BW 2 CATS
    dists = [[0 for _ in range(c)] for _ in range(c)]
    # QUEUE TO EVALUATE A CAT AND ITS DISTANCE
    q = PriorityQueue()
    # READ IN DISTANCE BETWEEN TWO CATS
    for k in range(num):
        i,j,d = map(int, stdin.readline().split())  
        dists[i][j] = d + 1
        dists[j][i] = d + 1
    # ADD IN DISTANCE 1 FOR CAT 0
    q.put((1,0))
    best = [5000 for _ in range(c)]
    best[0] = 1
    # FINAL MILK QUANTITY
    ans = 0
    while not q.empty():
        # GET EACH DISTANCE AND CAT
        (dist, cat) = q.get()
        # IF DISTANCE IS VALID
        if best[cat] == dist:
            # UPDATE MILK USED
            ans += dist
            best[cat] = 0
            # GET NEIGHBOR CATS
            for adj in range(c):
                # ADD CAT IF VALID
                if adj != cat and dists[cat][adj] < best[adj]:
                    best[adj] = dists[cat][adj]
                    q.put((dists[cat][adj], adj))
    # PRINT SOLUTION
    if ans <= m:
        print("yes")
    else :
        print("no")
    # print(ans)


    

