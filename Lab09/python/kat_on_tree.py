from sys import stdin

# READ IN BRANCH KITTEN IS CURRENTLY ON
k = int(stdin.readline())
# BRANCHING
val = stdin.readline().strip()
# BRANCH NODES
parents = [0 for _ in range(101)]
while val != '-1':
    current_vals = list(map(int,val.split(" ")))
    parent = current_vals[0]
    # STORE PARENT AND CHILDREN NODE CONNECTION
    for i in range(1,len(current_vals)):
        parents[current_vals[i]] = parent
    val = stdin.readline().strip()
print(k, end=' ')
# TRACE PATH
while parents[k] != 0:
    k = parents[k]
    print(k, end=' ')
print()
