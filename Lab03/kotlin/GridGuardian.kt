private fun modPow(n: Long, k: Long, m: Long): Long { // log(k)
    if (k == 1L) {
        return n
    }
    var half = modPow(n, k shr 1, m)
    half *= half
    if (k and 1L == 1L) {
        half %= m
        half *= n
    }
    return half % m
}

fun main() {
    val input = readLine()!!.split(" ").map { it.toInt() }
    var n = input[0]
    var m = input[1]
    val mod = input[2].toLong()

    if (n and 1 == 1 && m and 1 == 1) { // case 1 O(1)
        println(1)
    } else if (n and 1 == 1) { // case 2 O(log(n))
        println(modPow((m shr 1) + 1L, n.toLong() shr 1, mod))
    } else if (m and 1 == 1) { // case 3 O(log(m))
        println(modPow((n shr 1) + 1L, m.toLong() shr 1, mod))
    } else { // O(m(n^2)(2^(n/2)))
        n = n shr 1
        m = m shr 1

        val subsets = Array(n + 1) { Array(n + 1) { IntArray(1 shl n) } }
        for (prevPref in 0..n) { // O(n) prevPref is the previous column shift
            for (nextPref in 0..n) { // O(n) nextPref is the next column shift
                for (state in 0 until (1 shl n)) { // O(2^n) // next rowShift
                    var finalState = state
                    for (bad in prevPref + 1 until nextPref) { // O(n)
                        if (state and (1 shl (bad - 1)) == 0 && state and (1 shl bad) != 0) { // detecting open square situation
                            finalState = finalState xor (1 shl bad) // take away the bad bit
                        }
                    }
                    for (bad in nextPref until prevPref - 1) { // O(n)
                        if (state and (1 shl bad) != 0 && state and (1 shl (bad + 1)) == 0) { // detecting other open square situation
                            finalState = finalState xor (1 shl bad) // take away the bad bit
                        }
                    }
                    subsets[prevPref][nextPref][state] =
                        finalState  // this state is the subset that avoids the invalid arrangement
                }
            }
        }


        val dp = Array(2) { Array(n + 1) { LongArray(1 shl n) } }
        dp.first().forEach { it.fill(1) }
        var cur = 0

        repeat(m - 1) { // O(m)
            for (prefixA in 0..n) { // O(n) prefixA is previous column shift
                for (bit in 0 until n) { // O(n)
                    // sum over subsets, essentially 11 becomes the sum of what was previously in 00, 01, 10, 11
                    // the same idea extends to any set and its subsets
                    for (mask in 0 until (1 shl n)) { // O(2^(n/2)), mask is the set of row shifts of the previous column
                        if ((1 shl bit) and mask != 0) {
                            dp[cur][prefixA][mask] += dp[cur][prefixA][mask xor (1 shl bit)]
                        }
                    }
                }
            }

            // passing the data along to the next column
            for (prefixB in 0..n) { // O(n), prefixB is the column shift of the next column
                for (mask in 0 until (1 shl n)) { // O(2^(n/2))
                    dp[cur xor 1][prefixB][mask] = dp[cur][0][subsets[0][prefixB][mask]] % mod
                }
                for (prefixA in 1..n) { // O(n)
                    for (mask in 0 until (1 shl n)) { // O(2^(n/2)), mask is the set of row shifts of the next column
                        dp[cur xor 1][prefixB][mask] += dp[cur][prefixA][subsets[prefixA][prefixB][mask]] % mod
                        // the access to the subsets array here is getting the set of the previous column's row shifts that avoid invalid arrangements
                    }
                }
            }

            cur = cur xor 1
        }
        var ans = 0L
        for (i in 0..n) { // O(n) add up all the answers after processing all the columns
            for (state in 0 until (1 shl n)) { // O(2^n)
                ans += dp[cur][i][state]
            }
        }
        println(ans % mod)
    }
}