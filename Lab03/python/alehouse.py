import heapq
from sys import stdin

n,k = map(int, stdin.readline().split())

person_arr = []
for _ in range(n):
    a,b = map(int, stdin.readline().split())
    person_arr.append((a,b))

person_arr.sort()

heap = [] 

counter = 0
for a,b in person_arr:
    if len(heap) == 0:
        counter = max(counter, 1)
        heapq.heappush(heap, ((b,a), (a,b)))
    else:
        add_val = True
        while len(heap) > 0:
            val = heapq.heappop(heap)
            rear_pop = val[1][1]
            if a - rear_pop <= k:  
                heapq.heappush(heap, val)
                heapq.heappush(heap, ((b,a),(a,b)))
                counter = max(counter, len(heap))
                add_val = False
                break

        if add_val:
            heapq.heappush(heap,((b,a), (a,b)))
            counter = max(counter, 1)

print(counter)
