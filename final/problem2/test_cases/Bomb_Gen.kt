import java.io.PrintWriter
import kotlin.random.Random

fun main() { // random large test case
    val n = 100_000
    val c = Random.nextInt(1_000_000_001) // random bomb cost
    val treasures = IntArray(n) { Random.nextInt(1_000_000_001) } // random treasure values
    val parents = IntArray(n - 1) { i -> Random.nextInt(i + 1) } // random structure
    val shuffled = (0 until n - 1).shuffled() // random edge order
    val shuffled2 = (1..n).shuffled() // random renaming of nodes
    val file = PrintWriter("input file name")
    file.println("$n $c")
    file.println(treasures.joinToString(" "))
    for (i in shuffled) {
        if (Random.nextBoolean()) { // random order of nodes for each edge
            file.println("${shuffled2[i + 1]} ${shuffled2[parents[i]]}")
        } else {
            file.println("${shuffled2[parents[i]]} ${shuffled2[i + 1]}")
        }
    }
    file.close()
}