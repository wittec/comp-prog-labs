import java.io.PrintWriter
import kotlin.random.Random

fun main() { // all nodes parents are the entrance node
    val n = 100_000
    val c = 0 // made one with low cost, one with max cost
    val treasures = IntArray(n) { Random.nextInt(1_000_000_001) } // random treasure values
    val shuffled = (0 until n - 1).shuffled() // random ordering of the edges
    val file = PrintWriter("input file name")
    file.println("$n $c")
    file.println(treasures.joinToString(" "))
    for (i in shuffled) {
        if (Random.nextBoolean()) { // random ordring of nodes within edges
            file.println("${i + 2} 1")
        } else {
            file.println("1 ${i + 2}")
        }
    }
    file.close()
}