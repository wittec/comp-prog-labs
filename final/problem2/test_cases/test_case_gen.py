import random
file = open('final/problem2/test_cases/test3_in.txt', 'w')

file.write('100000 2\n')
arr = []
for i in range(100000):
    arr.append(random.randrange(0,1_000_000_001))
file.write(" ".join(map(str, arr)))
file.write("\n")

uv_arr = []
for i in range(100000 - 1):
    uv_arr.append(f'{i + 2} {random.randrange(1, i + 2)}')
file.write('\n'.join(uv_arr))


