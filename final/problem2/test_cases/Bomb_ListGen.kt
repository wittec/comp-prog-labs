import java.io.PrintWriter
import kotlin.random.Random

fun main() {
    val n = 100_000
    val c = 2 // made one with high c, one with small c
    val treasures = IntArray(n) { Random.nextInt(1_000_000_001) } // random values for the treasures
    val parents = IntArray(n - 1) { i -> i } // creates a list like structure
    val shuffled = (0 until n - 1).shuffled() // randomizes the order of the edges
    val shuffled2 = IntArray(n)
    (2..n).shuffled().toIntArray().copyInto(shuffled2, 1) // randomizes the label of each node except for room 1
    shuffled2[0] = 1
    val file = PrintWriter("input file name")
    file.println("$n $c")
    file.println(treasures.joinToString(" "))
    for (i in shuffled) {
        if (Random.nextBoolean()) { // randomizes the order of the nodes within the edge
            file.println("${shuffled2[i + 1]} ${shuffled2[parents[i]]}")
        } else {
            file.println("${shuffled2[parents[i]]} ${shuffled2[i + 1]}")
        }
    }
    file.close()
}