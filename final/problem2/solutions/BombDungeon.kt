import kotlin.math.max

fun main() {
    val br = System.`in`.bufferedReader()
    
    // read in number of nodes, bomb cost, and treasure values
    val (n, c) = br.readLine().split(" ").map { it.toInt() }
    val treasures = br.readLine().split(" ").map { it.toLong() }
    val cons = Array(n) { mutableListOf<Int>() }
    
    // reda in graph structure
    repeat(n - 1) {
        val (u, v) = br.readLine().split(" ").map { it.toInt() - 1 }
        cons[u].add(v)
        cons[v].add(u)
    }
    
    // this function returns the profit of entering this room
    fun dfs(node: Int, parent: Int): Long {
        // ifBomb is the profit you get from using a bomb in this room
        var ifBomb = -c.toLong()
        for (child in cons[node]) {
            if (child != parent) {
                // add the profit for every room you can now access by using a bomb in this room
                ifBomb += dfs(child, node)
            }
        }
        // if the profit from using the bomb is more than 0, use it. Also add the treasure from this room
        return max(ifBomb, 0L) + treasures[node]
    }
    // determing whether we even want to enter the dungeon at all
    println(max(dfs(0, -1) - c, 0L))
}