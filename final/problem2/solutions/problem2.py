from sys import stdin

n,c = map(int, stdin.readline().split())
val_arr = list(map(int, stdin.readline().split()))

conn = [[] for _ in range(n)]

for i in range(n - 1):
    u,v = map(int, stdin.readline().split())
    conn[u - 1].append(v - 1)
    conn[v - 1].append(u - 1)

def dfs(node, parent):
    new_cost = -c
    for child in conn[node]:
        if child != parent:
            new_cost += dfs(child, node)
    return val_arr[node] + max(new_cost, 0)

print(max(dfs(0,-1) - c, 0))
