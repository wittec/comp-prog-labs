import random

file = open("final/problem1/test_cases/test4_in.txt", "w")
file.write("10\n")

for _ in range(10):
    ls = []
    for i in range(1, 50001):
        ls.append(i)
    file.write("50000\n")
    file.write(" ".join(map(str, ls)))
    file.write("\n")

file.close()