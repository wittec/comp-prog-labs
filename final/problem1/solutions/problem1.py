from sys import stdin

t = int(stdin.readline())

for _ in range(t):
	n = int(stdin.readline())
	a = list(map(int, stdin.readline().split()))
 
	dp = [-1 for i in range(n)]
	check = [0 for i in range(n)]
	check[n-1] = 1
	dp[n-1] = 0
 
	d = {}
	for i in range(n):
		d[a[i]-1] = i
 
	l = [n-1]
	i = d[n-1]
	while(check[i] != 1):
		l.append(i)
		dp[i] = 0
		check[i] = 1
		i = d[i]
 
	count = 1
	while(0 in check):
		p = []
		for i in l:
			if i-1 >= 0 and check[i-1] == 0:
				check[i-1] = 1
				p.append(i-1)
				dp[i-1] = count
				j = d[i-1]
				while(check[j] != 1):
					p.append(j)
					dp[j] = count
					check[j] = 1
					j = d[j]
 
			if i+1 < n and check[i+1] == 0:
				check[i-1] = 1
				p.append(i+1)
				dp[i+1] = count
				j = d[i+1]
				while(check[j] != 1):
					p.append(j)
					dp[j] = count
					check[j] = 1
					j = d[j]
 
		l = p
		count += 1
	
	print(dp[0])