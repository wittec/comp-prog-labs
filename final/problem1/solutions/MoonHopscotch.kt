import java.util.ArrayDeque

fun main() {
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    // read in number of test cases
    val t = br.readLine().toInt()
    repeat(t) {
        val queue = ArrayDeque<Int>()
        val n = br.readLine().toInt()
        // read in permutaiton and make 0 indexed
        val nums = br.readLine().split(" ").map { it.toInt() - 1 }
        
        // set the distance values to a value higher than what's possible
        val dists = IntArray(n) { n }
        // distance to the first node is 0
        dists[0] = 0
        queue.addFirst(0)
        val processed = BooleanArray(n)
        while (queue.isNotEmpty()) {
            val node = queue.pollFirst()
            if (!processed[node]) { // if we've already processed the node, ignore it
                val dist = dists[node]
                processed[node] = true
                if (node > 0) { // check the node behind this node
                    if (dists[node - 1] > dist + 1) {
                        dists[node - 1] = dist + 1
                        queue.addLast(node - 1) // add to back because increased cost
                    }
                }
                if (node + 1 < n) { // check the node ahead of this node
                    if (dists[node + 1] > dist + 1) {
                        dists[node + 1] = dist + 1
                        queue.addLast(node + 1) // add to back because increased cost
                    }
                }
                if(dists[nums[node]] > dist) { // go to written step
                    dists[nums[node]] = dist
                    queue.addFirst(nums[node]) // add to beginning because 0 cost movement
                }
            }
        }
        // answer is the distance to the last step
        bw.append("${dists.last()}\n")
    }
    bw.flush()
}