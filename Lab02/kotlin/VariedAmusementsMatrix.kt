import java.io.BufferedReader

private fun matrixMult(a: Array<LongArray>, b: Array<LongArray>, mod: Long): Array<LongArray> {
    val n = a.size
    val nm = a[0].size
    val m = b[0].size
    val ans = Array(n) { LongArray(m) }
    for (row in 0 until n) {
        for (col in 0 until m) {
            for (k in 0 until nm) {
                ans[row][col] += a[row][k] * b[k][col]
                ans[row][col] %= mod
            }
        }
    }
    return ans
}

private val identity = Array(3) { row -> LongArray(3) { col -> if (row == col) 1 else 0 } }

private fun matrixPow(n: Array<LongArray>, k: Long, mod: Long): Array<LongArray> {
    if (k == 0L) {
        return identity
    }
    var half = matrixPow(n, k shr 1, mod)
    half = matrixMult(half, half, mod)
    if (k and 1L == 1L) {
        half = matrixMult(half, n, mod)
    }
    return half
}


fun main() {
    val mod = 1_000_000_007L
    val br = System.`in`.bufferedReader()
    val n = br.readInt().toLong()
    val typeCounts = LongArray(3) { br.readInt().toLong() }
    val matrix = Array(3) { LongArray(3) }
    for (i in 0 until 3) {
        for (j in 0 until 3) {
            if (i != j) {
                matrix[i][j] = typeCounts[i]
            }
        }
    }
    val res = matrixPow(matrix, n - 1, mod)
    val vec = Array(3) { idx -> LongArray(1) { typeCounts[idx] } }
    val ansMatrix = matrixMult(res, vec, mod)
    var ans = 0L
    for (i in 0 until 3) {
        ans += ansMatrix[i][0]
    }
    println(ans % mod)
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}