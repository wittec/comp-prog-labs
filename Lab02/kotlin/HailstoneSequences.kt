fun main() {
    val br = System.`in`.bufferedReader()
    var n = br.readLine().toLong()
    var ans = 1
    while (n != 1L) {
        if (n and 1L == 1L) {
            n = (3 * n + 1) shr 1
            ans += 2
        } else {
            n = n shr 1
            ans++
        }
    }
    println(ans)
}