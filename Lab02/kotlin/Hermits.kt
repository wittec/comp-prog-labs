import java.io.BufferedReader

fun main() {
    val br = System.`in`.bufferedReader()
    val n = br.readInt()
    val pops = LongArray(n) { br.readInt().toLong() }
    val quiet = LongArray(n) { i -> pops[i] }
    val m = br.readInt()
    repeat(m) {
        val u = br.readInt() - 1
        val v = br.readInt() - 1
        quiet[u] += pops[v]
        quiet[v] += pops[u]
    }
    var ans = 0
    for (i in 1 until n) {
        if (quiet[i] < quiet[ans]) {
            ans = i
        }
    }
    println(ans + 1)
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}