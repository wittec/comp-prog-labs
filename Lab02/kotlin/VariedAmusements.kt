import java.io.BufferedReader

fun main() {
    val mod = 1_000_000_007L
    val br = System.`in`.bufferedReader()
    val n = br.readInt()
    val typeCounts = IntArray(3) { br.readInt() }
    val prevs = LongArray(3) { i -> typeCounts[i].toLong() }
    val curs = LongArray(3)
    repeat(n - 1) {
        for (i in 0 until 3) {
            val j = (i + 1) % 3
            val k = (i + 2) % 3
            curs[i] = (typeCounts[i] * (prevs[j] + prevs[k])) % mod
        }
        for (i in 0 until 3) {
            prevs[i] = curs[i]
        }
    }
    println(prevs.sum() % mod)
}

private const val SPACE_INT = ' '.toInt()
private const val ZERO_INT = '0'.toInt()
private const val NL_INT = '\n'.toInt()

private fun BufferedReader.readInt(): Int {
    var ret = read()
    while (ret <= SPACE_INT) {
        ret = read()
    }
    val neg = ret == '-'.toInt()
    if (neg) {
        ret = read()
    }
    ret -= ZERO_INT
    var read = read()
    while (read >= ZERO_INT) {
        ret *= 10
        ret += read - ZERO_INT
        read = read()
    }

    while (read <= SPACE_INT && read != -1 && read != NL_INT) {
        mark(1)
        read = read()
    }
    if (read > SPACE_INT) {
        reset()
    }
    return if (neg) -ret else ret
}