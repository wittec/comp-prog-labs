fun main() {
    val mod = 1_000_000_007L
    val br = System.`in`.bufferedReader()
    val n = br.readLine().toInt()
    val cur = LongArray(4)
    val prev = LongArray(4)
    prev[0b01] = 1
    prev[0b11] = 1
    repeat(n - 1) {
        for (type in 0 until 2) {
            cur[type shl 1] = prev[(type shl 1) or 1]
            cur[(type shl 1) or 1] = (prev[type shl 1] + prev[((type xor 1) shl 1) or type]) % mod
        }
        for(i in 0 until 4){
            prev[i] = cur[i]
        }
    }
    println((prev[0b01] + prev[0b10]) % mod)
}