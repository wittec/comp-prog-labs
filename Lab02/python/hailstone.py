from sys import stdin

#Read in starting value
starting_value = int(stdin.readline())

current_value = starting_value
length = 1

#while the loop isn't at end value
while current_value != 1:
    #if even
    if current_value % 2 == 0:
        current_value /= 2
    #if odd
    else:
        current_value = current_value * 3 + 1
    length += 1
print(length)