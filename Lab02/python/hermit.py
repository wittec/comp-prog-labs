from sys import stdin

#read in starting values
n = int(stdin.readline())

population = list(map(int, stdin.readline().split()))
quiet = [population[i] for i in range(n)]

m = int(stdin.readline())

#Get m road crossings 
for i in range(m):
    cross1,cross2 = map(int, stdin.readline().split())
    quiet[cross1 - 1] += population[cross2 - 1]
    quiet[cross2 - 1] += population[cross1 - 1]

#Find index of lowest quiet value
ans = 0
for i in range(1, n):
    if quiet[i] < quiet[ans]:
        ans = i
print(ans + 1)

