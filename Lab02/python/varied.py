from sys import stdin

#overflow number
mod = 1_000_000_007

#read in and split starting vars
n,a,b,c = map(int, stdin.readline().split())
n_array = []

#create 2d array
for i in range(3):
    temp = [0 for i in range(n)]
    n_array.append(temp)

#fill first rows for ride types
n_array[0][0] = a
n_array[1][0] = b
n_array[2][0] = c
for i in range(1, n):
    #go to different rides that aren't same type
    n_array[0][i] = (a * (n_array[1][i - 1] + n_array[2][i - 1])) % mod
    n_array[1][i] = (b * (n_array[0][i - 1] + n_array[2][i - 1])) % mod
    n_array[2][i] = (c * (n_array[0][i - 1] + n_array[1][i - 1])) % mod
    
print((n_array[0][n - 1] + n_array[1][n - 1] + n_array[2][n - 1]) % mod)
