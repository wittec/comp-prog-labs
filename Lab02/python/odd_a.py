from sys import stdin

#Overflow value
mod = 1_000_000_007

#Read in first value
n = int(stdin.readline())

#Create dp array
dp = [[[0, 0], [0, 0]] for i in range(n)]
for ch in range(2):
    dp[0][ch][1] = 1

for i in range(1, n):
    #Go through flags and set to a or b accordingly
    dp[i][0][0] = dp[i - 1][0][1]
    dp[i][1][0] = dp[i - 1][1][1]
    dp[i][0][1] = (dp[i - 1][0][0] + dp[i - 1][1][0]) % mod
    dp[i][1][1] = (dp[i - 1][0][1] + dp[i - 1][1][0]) % mod

print((dp[n - 1][0][1] + dp[n - 1][1][0])% mod)

