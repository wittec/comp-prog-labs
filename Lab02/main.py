from sys import stdin
# Get inital data
n,k = map(int, stdin.readline().split())

time_arr = []
time_tup_arr = []
for i in range(n):
    a,b = map(int, stdin.readline().split())
    time_arr.append(a)
    time_arr.append(b)
    time_tup_arr.append((a,b))

sorted_arr = sorted(time_arr)
sorted_tup_arr = sorted(time_tup_arr)
#used = []
ans = 0
last = -1
i = 0
for i in sorted_arr:
    used = []
    while (i < n and sorted_tup_arr[i][0] > last and sorted_tup_arr[i][0] <= i + k):
        used.append(sorted_tup_arr[i][1])
        i += 1
    last = i + k
    ans = max(ans, len(used))
    
print(ans)