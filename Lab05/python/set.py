from sys import stdin

# ARRAY TO STORE CARDS
arr = []

# READ IN THE CARDS INFORMATION
for i in range(4):
    arr += stdin.readline().strip().split(" ")

# BOOLEAN VALUE TO TRACK IF ANY SET HAS BEEN FOUND
found = False
# LOOPS TO COMPARE 3 CARDS AT A TIME
for i in range(10):
    for j in range(i + 1, 11):
        for k in range(j + 1, 12):
            # BOOLEAN TO TRACK IF THE 3 CARDS FORM A SET
            good = True
            # FOR EACH CHARACTER IN CARD
            for x in range(4):
                if arr[i][x] != arr[j][x] or arr[j][x] != arr[k][x]:
                    # IF ANY VALUE IS THE SAME IN ALL THREE CARDS, ITS NOT PART OF SET
                    if arr[i][x] == arr[j][x] or arr[j][x] == arr[k][x] or arr[k][x] == arr[i][x]:
                        good = False
            # IF CONDITION IS SATISFIED, PRINT SET
            if good:
                found = True # UPDATE BOOL TO SET HAS BEEN FOUND
                print(f'{i + 1} {j + 1} {k + 1}')
if not found:
    print('no sets')



