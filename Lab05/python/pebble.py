from sys import stdin
from collections import deque

# READ IN NUMBER OF GAMES
m = int(stdin.readline())
for i in range(m):
    line = stdin.readline().strip()
    # RECORDS STATE OF GAME
    state = 0
    # RETURNS BINARY REPRESENTATION OF GAME STATE
    # 'O' -> 1, '-' -> 0
    for char in line:
        state <<= 1
        if char == 'o':
            state += 1
    # RECORDS WHETHER CURRENT STATE HAS BEEN VISITED OR NOT
    visited = [False for _ in range(1 << 12)]
    visited[state] = True
    # QUEUE TO STORE STATES OF THE GAME
    queue = deque()
    queue.append(state)
    # RECORDS NUMBER OF PEBBLES LEFT
    ans = 12
    while len(queue) != 0:
        # GET CURRENT STATE OF GAME
        state = queue.pop()
        # RECORD NUMBER OF PEBBLES IN THE STATE
        ans = min(ans, bin(state).count("1"))
        for mid in range(1, 11):
            # COMPARE CONSECUTIVE LOCATIONS
            if (state >> mid) & 1 == 1:
                # CHANGE PEBBLE STATE
                if((state >> (mid - 1)) & 1 != (state >> (mid + 1)) & 1):
                    delta = 1 << mid
                    delta |= 1 << (mid - 1)
                    delta |= 1 << (mid + 1)
                    new = state ^ delta
                    # ADD NEW STATE TO QUEUE
                    if not visited[new]:
                        visited[new] = True
                        queue.append(new)
    print(ans)
