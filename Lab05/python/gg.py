from sys import stdin

# TRACK HIGH AND LOW VALUES
high = 10
low = 1

# READ IN GUESS
guess = int(stdin.readline())
while guess != 0:
    # READ IN RESPONSE
    res = stdin.readline().strip()
    if res == "too high":
        # TRACK MINIMUM VALUE IF GUESS IS HIGH
        high = min(high,  guess - 1)
    elif res == "too low":
        # TRACK MAXIMUM VALUE IF GUESS IS LOW
        low = max(low, guess + 1)
    else: # END OF ONE GAME
        # CHECK IF THE RESPONSE WAS VALID, THAT IS, GUESS LIES BETWEEN MIN AND MAX
        # AND MIN IS LESS THAN MAX
        if low <= high and low <= guess and guess <= high:
            print("Stan may be honest")
        else:
            print("Stan is dishonest")
        # RESET VARIABLES FOR NEXT GAME
        low = 1
        high = 10
    # READ IN NEXT GUESS
    guess = int(stdin.readline())
