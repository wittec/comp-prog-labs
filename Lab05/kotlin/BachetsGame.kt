fun main() {
    val limit = 1_000_000
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    var line = br.readLine()
    val prev = IntArray(limit + 1) // reuse the same array of max size
    while (line != null) {
        val split = line.split(" ").map { it.toInt() }
        val n = split[0]
        val nums = split.drop(2).toIntArray()
        nums.sort()
        for (i in 1..n) {
            prev[i] = 0 // override the values from previous cases
            for (num in nums) {
                val prevSum = i - num
                if (prevSum >= 0) { 
                    if (prev[prevSum] == 0) { // if this puts your opponent in a losing state
                        prev[i] = num // set this state to a winning state
                    }
                } else {
                    break
                }
            }
        }
        if (prev[n] == 0) { // value of 0 means the state is losing, otherwise it's winning
            bw.append("Ollie wins\n")
        } else {
            bw.append("Stan wins\n")
        }
        line = br.readLine()
    }
    bw.flush()
}