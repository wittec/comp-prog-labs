
#Get inital data
num_friends,num_friendships = map(int, input().split(" "))

#Create dict and fill with default data
friend_volume = {}
friend_volume = dict.fromkeys(range(1,num_friends + 1), 0)
output = ""


for i in range(num_friendships):

    #Split friends and increase friend volume respectively 
    friend1,friend2 = map(int, input().split(" "))
    friend_volume[friend1] += 1
    friend_volume[friend2] += 1

for person in friend_volume.keys():

    #Calculate the market coeff and add to output
    market = friend_volume[person] - person
    output += f"{market} "
    
print(output)