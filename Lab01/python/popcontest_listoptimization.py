#Get inital data
num_friends,num_friendships = map(int, input().split(" "))

#Create list and filled with negative success factor of each person
market_coef = [-(i + 1) for i in range(num_friends)]
output = ""


for i in range(num_friendships):

    #Split friends and increase friend volume respectively 
    friend1,friend2 = map(int, input().split(" "))
    market_coef[friend1 - 1] += 1
    market_coef[friend2 - 1] += 1

for person in range(num_friends):

    #Calculate the market coeff and add to output
    output += f"{market_coef[person]} "
    
print(output)
