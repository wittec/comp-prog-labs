
window_width = 0
final_height = 0
current_largest_width = 0
current_largest_height = 0
current_width = 0

#Flag for window width or EOF
expect_one = True
#Flag for EOF
done = False

while not done:
    if expect_one:
        #Set the window width and flip flag
        window_width = int(input())
        expect_one = False
        if window_width == 0:
            done = True
    else:
        width,height = map(int, input().split(" "))
        #If we don't reach end of object indicator "-1 -1"
        if width != -1:
            #If we exceed the max window_width
            if current_width + width > window_width:
                #Add the largest height of the block in row and reset vars
                final_height += current_largest_height
                current_largest_height = 0
                current_width = 0
            #Add new block to next row
            current_width += width
            
            #Update the largest varibles if need be
            if current_width > current_largest_width:
                current_largest_width = current_width
            if height > current_largest_height:
                current_largest_height = height
        else:
            #Print values and reset variables at end of test case
            print(f"{current_largest_width} x {final_height + current_largest_height}") 
            expect_one = True    
            window_width = 0
            final_height = 0
            current_largest_width = 0
            current_largest_height = 0
            current_width = 0
        