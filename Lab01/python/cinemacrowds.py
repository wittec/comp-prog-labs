#Get the first row of data and convert to int
t_size,group_number = map(int, input().split(" "))

#Convert the string of group sizes to list
groups = input().split(" ")

#Counter for groups not accepted
not_accept = 0

#Current number of people in movie
current_size = 0


for group in groups:
    size = int(group)

    #If the sum of the new group and current sum is over the threshold
    if current_size + size > t_size:
        #reject the current group
        not_accept += 1
    else:
        current_size += size
print(not_accept)