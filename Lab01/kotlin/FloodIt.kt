import java.util.ArrayDeque

fun main() {
    val dRow = listOf(0, 1, 0, -1) // makes iterating over adjacent squares in a grid easier
    val dCol = listOf(1, 0, -1, 0)
    val br = System.`in`.bufferedReader()
    val bw = System.out.bufferedWriter()
    val t = br.readLine().toInt()

    // this function returns the number of squares for each new color we could choose
    // i.e. [new squares if we use color 1, new squares if we use color 2, .. , new squares if we use color 6]
    fun gatherData(grid: Array<IntArray>): IntArray { 
        val n = grid.size
        val originColor = grid[0][0]
        val queue = ArrayDeque<Pair<Int, Int>>()
        queue += 0 to 0
        val visited = Array(grid.size) { BooleanArray(grid.size) }
        visited[0][0] = true
        val counts = IntArray(6)
        while (queue.isNotEmpty()) {
            val (row, col) = queue.poll()
            if (grid[row][col] != originColor) counts[grid[row][col]]++ 
            // if we're visiting a color that wasn't our initial color, increment the new squares for the new color
            for (dir in 0 until 4) {
                val newRow = row + dRow[dir]
                val newCol = col + dCol[dir]
                if (newRow in 0 until n && newCol in 0 until n) {
                    if (!visited[newRow][newCol]) {
                        if (grid[row][col] == grid[newRow][newCol] || grid[row][col] == originColor) {
                            // only go to the next square if the current square is the initial color, or if the next square is the same color as the current square
                            queue += newRow to newCol
                            visited[newRow][newCol] = true
                        }
                    }
                }
            }
        }
        return counts
    }

    fun flood(grid: Array<IntArray>, newColor: Int) { // turns the origin and all connected squares of the same color to the new color
        val n = grid.size
        val originColor = grid[0][0]
        val queue = ArrayDeque<Pair<Int, Int>>()
        queue += 0 to 0
        val visited = Array(grid.size) { BooleanArray(grid.size) }
        visited[0][0] = true
        while (queue.isNotEmpty()) {
            val (row, col) = queue.poll()
            grid[row][col] = newColor // change the color to the new color
            for (dir in 0 until 4) {
                val newRow = row + dRow[dir]
                val newCol = col + dCol[dir]
                if (newRow in 0 until n && newCol in 0 until n) {
                    if (!visited[newRow][newCol]) {
                        if (originColor == grid[newRow][newCol]) { // only go to squares the same color as the initial color
                            queue += newRow to newCol
                            visited[newRow][newCol] = true
                        }
                    }
                }
            }
        }
    }
    repeat(t) { // for each test case
        val n = br.readLine().toInt()
        val grid = Array(n) { br.readLine().map { it - '1' }.toIntArray() } // subtract 1 to make it 0 indexed, easier for proccessing
        val outData = IntArray(6) // the output data, i.e. how many times we changed the origin to each color
        var data = gatherData(grid) 
        while (data.sum() != 0) {
            var bestidx = 0
            for (x in 1 until 6) { // find the best color
                if (data[x] > data[bestidx]) { // ties implicitly broken by lower id color
                    bestidx = x
                }
            }
            outData[bestidx]++
            flood(grid, bestidx)
            data = gatherData(grid)
        }
        bw.append("${outData.sum()}\n") // number of moves
        bw.append("${outData.joinToString(" ")}\n") // moves by each color
    }
    bw.flush()
}
