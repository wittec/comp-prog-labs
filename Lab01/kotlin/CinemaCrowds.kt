fun main() {
    val br = System.`in`.bufferedReader()
    var (n, _) = br.readLine().split(" ").map { it.toInt() }
    val sizes = br.readLine().split(" ").map { it.toInt() }
    var ans = 0
    for (size in sizes) {
        if (size <= n) {
            n -= size
        } else {
            ans++
        }
    }
    println(ans)
}