import java.util.*


fun main() {
    val br = System.`in`.bufferedReader()
    val numWords = br.readLine().toInt()
    val words = Array(numWords) {
        br.readLine()
    }
    val trie = TabooCorasick(200_001)
    trie.addAllAndBuild(words)
    val n = trie.numNodes
    // count of the incoming edges to each node
    val incoming = IntArray(n)
    // this represents the number of nodes that we can reach from the empty string that does involve nodes that correspond to at or after the end of a word
    var nonFinal = n
    // generate all of the edges in the graph
    val cons = Array(n) { node ->
        if (trie.hasWord[node]) { // basically ignore these nodes
            nonFinal--
            IntArray(2) { -1 }
        } else {
            IntArray(2) { edge -> // a node we care about
                val dest = trie.nextState(node, edge)
                incoming[dest]++
                dest
            }
        }
    }

    val order =
        IntArray(nonFinal) // weird way of doing a queue where we know the maximum number of nodes we'll process.
    // works faster because it avoids the boxed java Integer class
    var inIdx = 0
    var outIdx = 0
    var furthestNode = 0
    val prev = IntArray(n)
    prev[0] = -1
    val dists = IntArray(n) // longest distance to the node

    // the empty string node is the only one that could have nothing incoming
    if (incoming[0] == 0) {
        inIdx++ // taking advantage of the fact that the queue is filled with 0's by default
        // and distances filled with 0 by default
    }

    while (inIdx > outIdx) { // fancy way of checking if it's not empty
        val node = order[outIdx++]
        if (dists[node] > dists[furthestNode]) { // update our furthest node if this node is at a further distance
            furthestNode = node
        }
        for (edge in 0..1) { // do the 0 edges first
            val nx = cons[node][edge]
            if (!trie.hasWord[nx]) { // checking if it's a node we care about
                if (dists[nx] < dists[node] + 1) { // if the path from 0 to node to nx is longer than the longest known path to nx, update nx's data
                    dists[nx] = dists[node] + 1
                    prev[nx] = node
                }
                if (--incoming[nx] == 0) { // if we've processed all nodes pointing to nx, add it to the queue
                    order[inIdx++] = nx
                }
            }
        }
    }
    if (outIdx != nonFinal) { // there's a cycle, so we can create an infinitely long string
        println(-1)
    } else {
        val ans = IntArray(dists[furthestNode])
        var cur = furthestNode
        for (i in dists[furthestNode] - 1 downTo 0) { // walking back the maxDist to construct the string
            val p = prev[cur]
            if (cons[p][1] == cur) {
                ans[i] = 1
            }
            cur = p
        }
        println(ans.joinToString(""))
    }
}

/**
 * Add all the words you need with the add function
 * then call build failures once you're done setting up the dictionary
 *
 * You'll likely need to mess with countMatches to make it do exactly what you need for your current problem
 */
private class TabooCorasick(n: Int) {
    val trie = Array(n) { IntArray(ALPHA_SIZE) { -1 } }
    var numNodes = 1
    val hasWord = BooleanArray(n)
    val fail = IntArray(n) { -1 }

    fun addAllAndBuild(dict: Array<String>) {
        for (s in dict) {
            add(s)
        }
        buildFailures()
    }

    private fun add(s: String) {
        var curNode = 0
        var curDepth = 0
        var curIdx = 0
        while (curIdx != s.length) {
            val c = s[curIdx] - BASE_CHAR
            if (trie[curNode][c] == -1) {
                trie[curNode][c] = numNodes++

            }
            curNode = trie[curNode][c]
            curDepth++
            curIdx++
        }
        hasWord[curNode] = true
    }

    private fun buildFailures() {
        val queue = ArrayDeque<Int>()
        for (c in 0 until ALPHA_SIZE) {
            if (trie[0][c] == -1) {
                trie[0][c] = 0
            } else {
                fail[trie[0][c]] = 0
                queue += trie[0][c]
            }
        }
        while (queue.isNotEmpty()) {
            val curState = queue.poll()
            for (c in 0 until ALPHA_SIZE) {
                if (trie[curState][c] != -1) {
                    // if this node's parent occurs on/after the end of a word, then this one does too
                    hasWord[trie[curState][c]] = hasWord[trie[curState][c]] || hasWord[curState]

                    var failure = fail[curState]
                    while (trie[failure][c] == -1) {
                        failure = fail[failure]
                    }
                    failure = trie[failure][c]
                    fail[trie[curState][c]] = failure


                    // if the fallback state has a word, then this one does too
                    hasWord[trie[curState][c]] = hasWord[trie[curState][c]] || hasWord[failure]


                    queue += trie[curState][c]
                }
            }
        }
    }

    fun nextState(curState: Int, c: Int): Int {
        var ret = curState
        while (trie[ret][c] == -1) {
            ret = fail[ret]
        }
        trie[curState][c] = trie[ret][c]
        return trie[ret][c]
    }

    companion object {
        private const val ALPHA_SIZE = 2
        private const val BASE_CHAR = '0'
    }
}