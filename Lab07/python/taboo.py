from sys import stdin
from collections import defaultdict

MAX_STATES = 200_000
MAX_CHARACTERS = 2

out = [0] * (MAX_STATES + 1)
fail = [-1] * (MAX_STATES + 1)
goto = [[-1]* MAX_CHARACTERS for _ in range(MAX_STATES+1)]

n = int(stdin.readline())
str_arr = []

for i in range(n):
    current_str = stdin.readline().strip()
    str_arr.append(current_str)

def build_matching_machine():
    k = len(str_arr)
    states = 1
    for i in range(k):
        current_str = str_arr[i]
        current_state = 0
        for character in current_str:
            ch = int(character)
            if goto[current_state][ch] == -1:
                goto[current_state][ch] = states
                states += 1
            current_state = goto[current_state][ch]
        out[current_state] |= (1<<i)
    for ch in range(MAX_CHARACTERS):
        if goto[0][ch] == -1:
            goto[0][ch] = 0
        
    queue = []
    for ch in range(MAX_CHARACTERS):
        if goto[0][ch] != 0:
            fail[goto[0][ch]] = 0
            queue.append(goto[0][ch])

    while queue:
        state = queue.pop()
        for ch in range(MAX_CHARACTERS):
            if goto[state][ch] != -1:
                failure = fail[state]
                while goto[failure][ch] == -1:
                    failure = fail[failure]
                failure = goto[failure][ch]
                fail[goto[state][ch]] = failure
                out[goto[state][ch]]|= out[failure]
                queue.append(goto[state][ch])
    return states

def find_next_state(current_state, next_input):
    answer = current_state
    ch = int(next_input)
    while goto[answer][ch] == -1:
        answer = fail[answer]
    return goto[answer][ch]

def search_words(text):
    current_state = 0
    result = defaultdict(list)
    for i in range(len(text)):
        current_state = find_next_state(current_state, text[i])
        if out[current_state] == 0:
            continue
        for j in range(len(str_arr)):
            if (out[current_state] & (1 << j)) > 0:
                word = str_arr[j]
                result[word].append(i - len(word) + 1)
    return result


states_count = build_matching_machine()
print("hello")

